package main

import (
"fmt"
"log"
"net/http"
)

func main() {
  http.HandleFunc("/", handler)
  port := "80"
  if err := http.ListenAndServe(":" + port, nil); err != nil {
    log.Fatal(err)
  }
}

func handler(w http.ResponseWriter, r *http.Request) {
  fmt.Fprintf(w, "Hello 宝批龙!\n")
}